#pragma once

#include"invoke_interface.h"
#include"version.h"
#include"env.h"
#include"error.h"

namespace jni
{

class vm
{
	JavaVM *v;
public:
	jni::env env(version ver=version::v1_8)
	{
		JNIEnv* eptr;
		check_error_code((*v)->GetEnv(v,(void**)&eptr,static_cast<int>(ver)));
		return eptr;
	}
	auto version()
	{
		return jni::version::v1_8;
	}
};

}

/*extern "C"
{
JNIEXPORT jni::version JNICALL JNI_OnLoad(jni::vm vm, void *reserved);
JNIEXPORT void JNICALL JNI_OnUnload(jni::vm vm, void *reserved);
}*/