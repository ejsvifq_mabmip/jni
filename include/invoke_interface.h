#pragma once

#include"type.h"

namespace jni
{
struct invoke_interface
{
	using JavaVM = const invoke_interface*;
	void *reserved0;
	void *reserved1;
	void *reserved2;

	jint (JNICALL *DestroyJavaVM)(JavaVM *vm);

	jint (JNICALL *AttachCurrentThread)(JavaVM *vm, void **penv, void *args);

	jint (JNICALL *DetachCurrentThread)(JavaVM *vm);

	jint (JNICALL *GetEnv)(JavaVM *vm, void **penv, jint version);

	jint (JNICALL *AttachCurrentThreadAsDaemon)(JavaVM *vm, void **penv, void *args);
};

using JavaVM = invoke_interface::JavaVM;
}

extern "C"
{

#ifdef _JNI_IMPLEMENTATION_
#define _JNI_IMPORT_OR_EXPORT_ JNIEXPORT
#else
#define _JNI_IMPORT_OR_EXPORT_ JNIIMPORT
#endif

_JNI_IMPORT_OR_EXPORT_ jni::jint JNICALL
JNI_GetDefaultJavaVMInitArgs(void *args);

_JNI_IMPORT_OR_EXPORT_ jni::jint JNICALL
JNI_CreateJavaVM(jni::JavaVM **pvm, void **penv, void *args);

_JNI_IMPORT_OR_EXPORT_ jni::jint JNICALL
JNI_GetCreatedJavaVMs(jni::JavaVM **, jni::jsize, jni::jsize *);

}