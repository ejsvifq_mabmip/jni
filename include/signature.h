#pragma once

#include"type.h"
#include<array>
#include<experimental/array>
#include<stdexcept>

namespace jni
{
template<typename> struct signature;
template<> struct signature<jboolean>
{
	static constexpr auto name=std::experimental::to_array("Z");
};
template<> struct signature<jbyte>
{
	static constexpr auto name=std::experimental::to_array("B");
};
template<> struct signature<jchar>
{
	static constexpr auto name=std::experimental::to_array("C");
};
template<> struct signature<jshort>
{
	static constexpr auto name=std::experimental::to_array("S");
};
template<> struct signature<jint>
{
	static constexpr auto name=std::experimental::to_array("I");
};
template<> struct signature<jlong>
{
	static constexpr auto name=std::experimental::to_array("J");
};
template<> struct signature<jfloat>
{
	static constexpr auto name=std::experimental::to_array("F");
};
template<> struct signature<jdouble>
{
	static constexpr auto name=std::experimental::to_array("D");
};
template<> struct signature<void>
{
	static constexpr auto name=std::experimental::to_array("V");
};

template<> struct signature<jstring>
{
	static constexpr auto name=std::experimental::to_array("Ljava/lang/String;");
};

namespace details
{

template<typename U,typename ...Args>
struct multisignature
{
	static constexpr auto calcuate()
	{
		constexpr auto a(multisignature<Args...>::calcuate());
		constexpr auto asize(a.size());
		using sig = signature<U>;
		constexpr auto usize(sig::name.size());
		std::array<char,usize+asize-1> res={};
		for(std::size_t i(0);i!=usize-1;++i)
			res.at(i)=sig::name.at(i);
		for(std::size_t i(0);i!=asize;++i)
			res.at(i+usize-1)=a.at(i);
		return res;
	}
};

template<typename U>
struct multisignature<U>
{
	static constexpr auto calcuate()
	{
		return signature<U>::name;
	}	
};

}

template<typename R,typename... Args>
struct signature<R(Args...)>
{
private:
	static constexpr auto calculate_name()
	{
		constexpr auto multi(details::multisignature<Args...>::calcuate());
		if(!multi.size())
			throw std::runtime_error("invalid signature function paramaters");
		using sig = signature<R>;
		std::array<char,multi.size()+sig::name.size()+1> res={'('};
		for(std::size_t i(0);i!=multi.size()-1;++i)
			res.at(i+1)=multi.at(i);
		res.at(multi.size())=')';
		for(std::size_t i(0);i!=sig::name.size();++i)
			res.at(multi.size()+1+i)=sig::name.at(i);
		return res;
	}
public:
	static constexpr auto name = calculate_name();
};

template<typename R>
struct signature<R()>
{
private:
	static constexpr auto calculate_name()
	{
		using sig = signature<R>;
		std::array<char,sig::name.size()+2> res={'(',')'};
		for(std::size_t i(0);i!=sig::name.size();++i)
			res.at(i+2)=sig::name.at(i);
		return res;
	}
public:
	static constexpr auto name = calculate_name();
};

template<typename T>
constexpr auto get_signature()
{
	return signature<T>::name.data();
}

}