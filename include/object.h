#pragma once

#include"env.h"
#include"signature.h"

namespace jni
{
template<typename T>
class jniobject
{
	env e;
	jclass j;
	jobject o;
	T t;
public:
	template<typename ...Args>
	jniobject(env _e,Args &&...args):e(_e),j(e.find_class(T::name())),o(e.new_object(j,e.get_method_id(j,"<init>",signature<void(Args...)>::name.data()),std::forward<Args>(args)...))
	{}
	template<typename F,typename ...Args>
	F call(const char* n,Args &&...args)
	{
		return e.call_method<F>(o,e.get_method_id(j,n,signature<F(Args...)>::name.data()),std::forward<Args>(args)...);
	}
	auto operator->()
	{
		return &t;
	}
};
}