#pragma once
#include<string_view>
#include<string>
#include<locale>
#include"error.h"
#include"env.h"

namespace jni
{
class string
{
	env e;
	jstring prompt;
	const wchar_t* ptr;
public:
	string(env e_,jstring s):e(e_),prompt(s),ptr(e.get_string_chars(s)){}
	~string()
	{
		e.release_string_chars(prompt,ptr);
	}
	string(const string&)=delete;
	string& operator=(const string&)=delete;
	string& operator=(string&& b) noexcept
	{
		if(&b!=this)
		{
			e.release_string_chars(prompt,ptr);
			e=b.e;
			prompt=b.prompt;
			ptr=b.ptr;
			b.ptr=nullptr;
		}
		return *this;
	}
	string(string&& b) noexcept:e(b.e),prompt(b.prompt),ptr(b.ptr)
	{
		b.ptr=nullptr;
	}
	auto data() const
	{
		return ptr;
	}
	std::size_t size()
	{
		return e.get_string_length(prompt);
	}
	auto wstring_view()
	{
		return std::wstring_view(ptr,size());
	}
	auto wstring()
	{
		return std::wstring(ptr,size());
	}
};

class stringutf
{
	env e;
	jstring prompt;
	const char* ptr;
public:
	stringutf(env e_,jstring s):e(e_),prompt(s),ptr(e.get_string_utf_chars(s))
	{
	}
	~stringutf()
	{
		if(ptr)
			e.release_string_utf_chars(prompt,ptr);
	}
	stringutf(const stringutf&)=delete;
	stringutf& operator=(const stringutf&)=delete;
	stringutf& operator=(stringutf&& b) noexcept
	{
		if(&b!=this)
		{
			if(ptr)
				e.release_string_utf_chars(prompt,ptr);
			e=b.e;
			prompt=b.prompt;
			ptr=b.ptr;
			b.ptr=nullptr;
		}
		return *this;
	}
	stringutf(stringutf&& b) noexcept:e(b.e),prompt(b.prompt),ptr(b.ptr)
	{
		b.ptr=nullptr;
	}
	std::size_t size()
	{
		return e.get_string_utf_length(prompt);
	}
	auto data() const
	{
		return ptr;
	}
	auto string_view()
	{
		return std::string_view(ptr,size());
	}
	auto string()
	{
		return std::string(ptr,size());
	}
};

class newstring
{
	env e;
	jstring j;
public:
	template<typename ...Args>
	newstring(env e_,Args&& ...args):e(e_),j(e_.new_string(std::forward<Args>(args)...)){}
	newstring(const newstring&)=delete;
	newstring& operator=(const newstring&)=delete;
	newstring(newstring&& b) noexcept:e(b.e),j(b.j)
	{
		b.j={};
	}
	newstring& operator=(newstring&& b) noexcept
	{
		if(&b!=this)
		{
			e.delete_local_ref(j);
			e=b.e;
			b.j={};
		}
		return *this;
	}
	~newstring()
	{
		e.delete_local_ref(j);
	}
	auto release()
	{
		auto temp(j);
		j = {};
		return temp;
	}
};

}