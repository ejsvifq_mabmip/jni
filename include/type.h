#pragma once

#include<cstddef>
#include<cstdint>

namespace jni
{
using jboolean=bool;
using jbyte=std::int8_t;
using jchar=wchar_t;
using jshort=std::int16_t;
using jint=std::int32_t;
using jlong=std::int64_t;
using jfloat = float;
using jdouble = double;
using jsize = std::size_t;

struct jobject
{
	void *handle = nullptr;
	operator bool() const
	{
		return handle;
	}
	jobject() = default;
};
class jclass : public jobject {};
class jthrowable : public jobject {};
class jstring : public jobject {};
class jarray : public jobject {};
class jbooleanArray : public jarray {};
class jbyteArray : public jarray {};
class jcharArray : public jarray {};
class jshortArray : public jarray {};
class jintArray : public jarray {};
class jlongArray : public jarray {};
class jfloatArray : public jarray {};
class jdoubleArray : public jarray {};
class jobjectArray : public jarray {};
class jweak : public jobject{};

union jvalue
{
	jboolean z;
	jbyte    b;
	jchar    c;
	jshort   s;
	jint     i;
	jlong    j;
	jfloat   f;
	jdouble  d;
	jobject  l;
};

struct jfieldID
{
	void *handle;
};

struct jmethodID
{
	void *handle;
};

enum class jobjectRefType
{
	invalid_ref_type,
	local_ref_type,
	global_ref_type,
	weak_global_ref_type
};

struct JNINativeMethod
{
	char *name;
	char *signature;
	void *fnPtr;
};


}