#pragma once

#include"native_interface.h"

namespace jni
{
template<typename T> struct call_method_helper;


template<>
struct call_method_helper<void>
{
	template<typename ...Args>
	auto operator()(JNIEnv *p,Args&& ...args)
	{
		return (*p)->CallVoidMethod(p,std::forward<Args>(args)...);
	}
};

template<>
struct call_method_helper<jobject>
{
	template<typename ...Args>
	auto operator()(JNIEnv *p,Args&& ...args)
	{
		return (*p)->CallObjectMethod(p,std::forward<Args>(args)...);
	}
};


template<>
struct call_method_helper<jboolean>
{
	template<typename ...Args>
	auto operator()(JNIEnv *p,Args&& ...args)
	{
		return (*p)->CallBooleanMethod(p,std::forward<Args>(args)...);
	}
};

template<>
struct call_method_helper<jbyte>
{
	template<typename ...Args>
	auto operator()(JNIEnv *p,Args&& ...args)
	{
		return (*p)->CallByteMethod(p,std::forward<Args>(args)...);
	}
};


template<>
struct call_method_helper<jchar>
{
	template<typename ...Args>
	auto operator()(JNIEnv *p,Args&& ...args)
	{
		return (*p)->CallCharMethod(p,std::forward<Args>(args)...);
	}
};

template<>
struct call_method_helper<jshort>
{
	template<typename ...Args>
	auto operator()(JNIEnv *p,Args&& ...args)
	{
		return (*p)->CallShortMethod(p,std::forward<Args>(args)...);
	}
};

template<>
struct call_method_helper<jint>
{
	template<typename ...Args>
	auto operator()(JNIEnv *p,Args&& ...args)
	{
		return (*p)->CallIntMethod(p,std::forward<Args>(args)...);
	}
};

template<>
struct call_method_helper<jlong>
{
	template<typename ...Args>
	auto operator()(JNIEnv *p,Args&& ...args)
	{
		return (*p)->CallLongMethod(p,std::forward<Args>(args)...);
	}
};

template<>
struct call_method_helper<jfloat>
{
	template<typename ...Args>
	auto operator()(JNIEnv *p,Args&& ...args)
	{
		return (*p)->CallFloatMethod(p,std::forward<Args>(args)...);
	}
};

template<>
struct call_method_helper<jdouble>
{
	template<typename ...Args>
	auto operator()(JNIEnv *p,Args&& ...args)
	{
		return (*p)->CallDoubleMethod(p,std::forward<Args>(args)...);
	}
};

template<typename T> struct call_nonvirtual_method_helper;


template<>
struct call_nonvirtual_method_helper<void>
{
	template<typename ...Args>
	auto operator()(JNIEnv *p,Args&& ...args)
	{
		return (*p)->CallNonvirtualVoidMethod(p,std::forward<Args>(args)...);
	}
};

template<>
struct call_nonvirtual_method_helper<jobject>
{
	template<typename ...Args>
	auto operator()(JNIEnv *p,Args&& ...args)
	{
		return (*p)->CallNonvirtualObjectMethod(p,std::forward<Args>(args)...);
	}
};


template<>
struct call_nonvirtual_method_helper<jboolean>
{
	template<typename ...Args>
	auto operator()(JNIEnv *p,Args&& ...args)
	{
		return (*p)->CallNonvirtualBooleanMethod(p,std::forward<Args>(args)...);
	}
};

template<>
struct call_nonvirtual_method_helper<jbyte>
{
	template<typename ...Args>
	auto operator()(JNIEnv *p,Args&& ...args)
	{
		return (*p)->CallNonvirtualByteMethod(p,std::forward<Args>(args)...);
	}
};


template<>
struct call_nonvirtual_method_helper<jchar>
{
	template<typename ...Args>
	auto operator()(JNIEnv *p,Args&& ...args)
	{
		return (*p)->CallNonvirtualCharMethod(p,std::forward<Args>(args)...);
	}
};

template<>
struct call_nonvirtual_method_helper<jshort>
{
	template<typename ...Args>
	auto operator()(JNIEnv *p,Args&& ...args)
	{
		return (*p)->CallNonvirtualShortMethod(p,std::forward<Args>(args)...);
	}
};

template<>
struct call_nonvirtual_method_helper<jint>
{
	template<typename ...Args>
	auto operator()(JNIEnv *p,Args&& ...args)
	{
		return (*p)->CallNonvirtualIntMethod(p,std::forward<Args>(args)...);
	}
};

template<>
struct call_nonvirtual_method_helper<jlong>
{
	template<typename ...Args>
	auto operator()(JNIEnv *p,Args&& ...args)
	{
		return (*p)->CallNonvirtualLongMethod(p,std::forward<Args>(args)...);
	}
};

template<>
struct call_nonvirtual_method_helper<jfloat>
{
	template<typename ...Args>
	auto operator()(JNIEnv *p,Args&& ...args)
	{
		return (*p)->CallNonvirtualFloatMethod(p,std::forward<Args>(args)...);
	}
};

template<>
struct call_nonvirtual_method_helper<jdouble>
{
	template<typename ...Args>
	auto operator()(JNIEnv *p,Args&& ...args)
	{
		return (*p)->CallNonvirtualDoubleMethod(p,std::forward<Args>(args)...);
	}
};



template<typename T> struct call_static_method_helper;


template<>
struct call_static_method_helper<void>
{
	template<typename ...Args>
	auto operator()(JNIEnv *p,Args&& ...args)
	{
		return (*p)->CallStaticVoidMethod(p,std::forward<Args>(args)...);
	}
};

template<>
struct call_static_method_helper<jobject>
{
	template<typename ...Args>
	auto operator()(JNIEnv *p,Args&& ...args)
	{
		return (*p)->CallStaticObjectMethod(p,std::forward<Args>(args)...);
	}
};


template<>
struct call_static_method_helper<jboolean>
{
	template<typename ...Args>
	auto operator()(JNIEnv *p,Args&& ...args)
	{
		return (*p)->CallStaticBooleanMethod(p,std::forward<Args>(args)...);
	}
};

template<>
struct call_static_method_helper<jbyte>
{
	template<typename ...Args>
	auto operator()(JNIEnv *p,Args&& ...args)
	{
		return (*p)->CallStaticByteMethod(p,std::forward<Args>(args)...);
	}
};


template<>
struct call_static_method_helper<jchar>
{
	template<typename ...Args>
	auto operator()(JNIEnv *p,Args&& ...args)
	{
		return (*p)->CallStaticCharMethod(p,std::forward<Args>(args)...);
	}
};

template<>
struct call_static_method_helper<jshort>
{
	template<typename ...Args>
	auto operator()(JNIEnv *p,Args&& ...args)
	{
		return (*p)->CallStaticShortMethod(p,std::forward<Args>(args)...);
	}
};

template<>
struct call_static_method_helper<jint>
{
	template<typename ...Args>
	auto operator()(JNIEnv *p,Args&& ...args)
	{
		return (*p)->CallStaticIntMethod(p,std::forward<Args>(args)...);
	}
};

template<>
struct call_static_method_helper<jlong>
{
	template<typename ...Args>
	auto operator()(JNIEnv *p,Args&& ...args)
	{
		return (*p)->CallStaticLongMethod(p,std::forward<Args>(args)...);
	}
};

template<>
struct call_static_method_helper<jfloat>
{
	template<typename ...Args>
	auto operator()(JNIEnv *p,Args&& ...args)
	{
		return (*p)->CallStaticFloatMethod(p,std::forward<Args>(args)...);
	}
};

template<>
struct call_static_method_helper<jdouble>
{
	template<typename ...Args>
	auto operator()(JNIEnv *p,Args&& ...args)
	{
		return (*p)->CallStaticDoubleMethod(p,std::forward<Args>(args)...);
	}
};



}