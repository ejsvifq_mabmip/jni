#pragma once
#include<stdexcept>
#include<utility>
#include"native_interface.h"
#include"version.h"
#include"call_method.h"
#include"type.h"
#include<new>
#include<string>

namespace jni
{
class env
{
	JNIEnv *p;
public:
	env(JNIEnv *ev):p(ev)
	{
		if(p==nullptr)
			throw std::runtime_error("JNIEnv* is a nullptr");
	}
	auto native_handle()
	{
		return p;
	}
	auto get_string_chars(jstring j,jboolean is_copy=false)
	{
		std::bad_alloc alloc;
		auto ptr((*p)->GetStringChars(p,j,&is_copy));
		if(ptr==nullptr)
			throw alloc;
		return ptr;
	}
	auto get_string_utf_chars(jstring j,jboolean is_copy=false)
	{
		std::bad_alloc alloc;
		auto ptr((*p)->GetStringUTFChars(p,j,&is_copy));
		if(ptr==nullptr)
			throw alloc;
		return ptr;
	}

	template<typename ...Args>
	auto new_object(Args&& ...args)
	{
		return (*p)->NewObject(p,std::forward<Args>(args)...);
	}
	
	template<typename ...Args>
	void release_string_chars(Args&& ...args)
	{
		(*p)->ReleaseStringChars(p,std::forward<Args>(args)...);
	}
	template<typename ...Args>
	auto get_string_length(Args&& ...args)
	{
		return (*p)->GetStringLength(p,std::forward<Args>(args)...);
	}
	
	template<typename ...Args>
	void release_string_utf_chars(Args&& ...args)
	{
		(*p)->ReleaseStringUTFChars(p,std::forward<Args>(args)...);
	}
	template<typename ...Args>
	auto get_string_utf_length(Args&& ...args)
	{
		return (*p)->GetStringUTFLength(p,std::forward<Args>(args)...);
	}
	auto exception_check()
	{
		return (*p)->ExceptionCheck(p);
	}
	void throw_new(jclass c,const char* m)
	{
		using namespace std::string_literals;
		if((*p)->ThrowNew(p,c,m)<0)
			throw std::runtime_error("jni throw_new error :"s+m);
	}
	void throw_new(jclass c,const std::string &m)
	{
		using namespace std::string_literals;
		if((*p)->ThrowNew(p,c,m.c_str())<0)
			throw std::runtime_error("jni throw_new error :"s+m);
	}

	void fatal_error(const char* m)
	{
		(*p)->FatalError(p,m);
	}
	void fatal_error(const std::string &m)
	{
		fatal_error(m.c_str());
	}

	auto find_class(const char* m)
	{
		return (*p)->FindClass(p,m);
	}
	auto find_class(const std::string &m)
	{
		return find_class(m.c_str());
	}
	jstring new_string(const char* m)
	{
		return (*p)->NewStringUTF(p,m);
	}	
	jstring new_string(const std::wstring& m)
	{
		return (*p)->NewString(p,m.data(),m.size());
	}
	jstring new_string(const std::string& m)
	{
		return new_string(m.c_str());
	}
	void delete_local_ref(const jobject &o)
	{
		return (*p)->DeleteLocalRef(p,o);
	}
	auto get_version() const
	{
		return static_cast<jni::version>((*p)->GetVersion(p));
	}
	auto get_object_class(const jobject &o)
	{
		return (*p)->GetObjectClass(p,o);
	}
	template<typename ...Args>
	auto get_method_id(Args&& ...args)
	{
		auto id((*p)->GetMethodID(p,std::forward<Args>(args)...));
		if(id.handle)
			return id;
		throw std::runtime_error("jni get_method_id failed");
	}
	template<typename ...Args>
	auto get_static_method_id(Args&& ...args)
	{
		auto id((*p)->GetStaticMethodID(p,std::forward<Args>(args)...));
		if(id.handle)
			return id;
		throw std::runtime_error("jni get_static_method_id failed");
	}
	
	template<typename T,typename ...Args>
	T call_method(Args&& ...args)
	{
		return call_method_helper<T>()(p,std::forward<Args>(args)...);
	}
	
	template<typename T,typename ...Args>
	T call_nonvirtual_method(Args&& ...args)
	{
		return call_nonvirtual_method_helper<T>()(p,std::forward<Args>(args)...);
	}
	
	template<typename T,typename ...Args>
	T call_static_method(Args&& ...args)
	{
		return call_static_method_helper<T>()(p,std::forward<Args>(args)...);
	}
	
	template<typename ...Args>
	auto get_field_id(Args &&...args)
	{
		return (*p)->GetFieldID(p,std::forward<Args>(args)...);
	}
	
	template<typename T>
	T get_field(const jobject &obj, jfieldID fieldID);

	void set_field(const jobject &obj, jfieldID fieldID,jobject o)
	{
		return (*p)->SetObjectField(p,obj,fieldID,o);
	}
	void set_field(const jobject &obj, jfieldID fieldID,jboolean o)
	{
		return (*p)->SetBooleanField(p,obj,fieldID,o);
	}
	void set_field(const jobject &obj, jfieldID fieldID,jbyte o)
	{
		return (*p)->SetByteField(p,obj,fieldID,o);
	}
	void set_field(const jobject &obj, jfieldID fieldID,jchar o)
	{
		return (*p)->SetCharField(p,obj,fieldID,o);
	}
	void set_field(const jobject &obj, jfieldID fieldID,jshort o)
	{
		return (*p)->SetShortField(p,obj,fieldID,o);
	}
	void set_field(const jobject &obj, jfieldID fieldID,jint o)
	{
		return (*p)->SetIntField(p,obj,fieldID,o);
	}
	void set_field(const jobject &obj, jfieldID fieldID,jlong o)
	{
		return (*p)->SetLongField(p,obj,fieldID,o);
	}
	void set_field(const jobject &obj, jfieldID fieldID,jfloat o)
	{
		return (*p)->SetFloatField(p,obj,fieldID,o);
	}
	void set_field(const jobject &obj, jfieldID fieldID,jdouble o)
	{
		return (*p)->SetDoubleField(p,obj,fieldID,o);
	}
	
	auto get_module(const jclass &c)
	{
		return (*p)->GetModule(p,c);
	}
};

template<>
jobject env::get_field<jobject>(const jobject &obj, jfieldID fieldID)
{
	return (*p)->GetObjectField(p,obj,fieldID);
}
template<>
jboolean env::get_field<jboolean>(const jobject &obj, jfieldID fieldID)
{
	return (*p)->GetBooleanField(p,obj,fieldID);
}

template<>
jbyte env::get_field<jbyte>(const jobject &obj, jfieldID fieldID)
{
	return (*p)->GetByteField(p,obj,fieldID);
}

template<>
jchar env::get_field<jchar>(const jobject &obj, jfieldID fieldID)
{
	return (*p)->GetCharField(p,obj,fieldID);
}

template<>
jshort env::get_field<jshort>(const jobject &obj, jfieldID fieldID)
{
	return (*p)->GetShortField(p,obj,fieldID);
}
template<>
jint env::get_field<jint>(const jobject &obj, jfieldID fieldID)
{
	return (*p)->GetIntField(p,obj,fieldID);
}
template<>
jlong env::get_field<jlong>(const jobject &obj, jfieldID fieldID)
{
	return (*p)->GetLongField(p,obj,fieldID);
}
template<>
jfloat env::get_field<jfloat>(const jobject &obj, jfieldID fieldID)
{
	return (*p)->GetFloatField(p,obj,fieldID);
}
template<>
jdouble env::get_field<jdouble>(const jobject &obj, jfieldID fieldID)
{
	return (*p)->GetDoubleField(p,obj,fieldID);
}
}