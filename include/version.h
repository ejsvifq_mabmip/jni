#pragma once

namespace jni
{
enum class version
{
	v1_1=0x00010001,
	v1_2=0x00010004,
	v1_6=0x00010006,
	v1_8=0x00010008,
	v9=0x00090000,
};

template<typename ostrm>
ostrm& operator<<(ostrm& os,const version& v)
{
	switch(v)
	{
	case version::v1_1:	return os<<"jni 1.1";
	case version::v1_2:	return os<<"jni 1.2";
	case version::v1_6:	return os<<"jni 1.6";
	case version::v1_8:	return os<<"jni 1.8";
	case version::v9:	return os<<"jni 9";
	default:	return os<<"unknown jni version";
	}
}

}