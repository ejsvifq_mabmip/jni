#pragma once
#include "type.h"
#include <system_error>
#include <string>
#include "env.h"

namespace jni
{
enum class error
{
	einval = -6,
	eexist,
	enomem,
	eversion,
	edetached,
	err,
	ok,
	commit,
	abort,
};
}

namespace std
{
template <> struct is_error_code_enum<jni::error> : public true_type {};
}

namespace jni
{
inline const std::error_category& error_category()
{
	class impl : public std::error_category
	{
	public:
		const char* name() const noexcept override
		{
			return "JNI";
		}
		std::string message(int ev) const override
		{
			using namespace std::string_literals;
			switch(static_cast<error>(ev))
			{
			case error::einval:
				return "Invalid arguments"s;
			case error::eexist:
				return "VM already created"s;
			case error::enomem:
				return "Not enough memory"s;
			case error::eversion:
				return "JNI version error"s;
			case error::edetached:
				return "Thread detached from the VM"s;
			case error::err:
				return "Unspecified error"s;
			case error::ok:
				return "Success"s;
			case error::commit:
				return "Commit"s;
			case error::abort:
				return "Abort"s;
			default:
				return "Unknown"s;
			}
		}
	};
	static impl skeleton;
	return skeleton;
}

inline void check_error_code(jint err)
{
	if (err) throw std::system_error(err, error_category());
}

class pending_java_exception {};
class fatal_error:public std::runtime_error
{
public:
	explicit fatal_error(const std::string &message):std::runtime_error(message){}
};

inline void check_java_exception(env& e)
{
	if (e.exception_check()) throw pending_java_exception();
}

inline jclass java_error_class(env& e)
{
	return e.find_class("java/lang/Error");
}

inline void throw_java_error(env& en)
{
try
{
	throw;
}
catch (const pending_java_exception&){}
catch (const fatal_error& e)
{
	en.fatal_error(e.what());
}
catch (const std::exception& e)
{
	en.throw_new(java_error_class(en), e.what());
}
catch (...)
{
	en.throw_new(java_error_class(en), "unknown native exception");
}
}

}
